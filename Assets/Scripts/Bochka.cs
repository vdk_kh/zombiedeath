﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bochka : MonoBehaviour {
	
	public GameObject expl;

	void OnTriggerEnter(Collider other){
		
		if (other.tag == "Zombie") {
			GameObject c = Instantiate (expl);
			c.transform.position = this.transform.position;
			Destroy (this.gameObject);
		}
	}
}