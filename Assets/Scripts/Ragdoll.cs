﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour {

	Rigidbody rb;

	void Start () {
		rb = this.GetComponentInChildren<Rigidbody> ();
		rb.AddForce (Random.Range(-20000,20000), Random.Range(10000,20000), Random.Range(-20000,20000));
	}
}
