﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public GameObject bochka;
	public GameObject dayText;
	public GameObject nightText;
	public GameObject zLight;
	public GameObject brain;

	public GameObject[] palisades;
	public Vector3[] targets;
	public Zombie[] zombs;

	public Text txBochka;

	public int sum;
	int numBochka = 4;
	int kolvo;

	float timer;


	public void toNight(){
		RenderSettings.ambientLight = Color.black;
		nightText.SetActive (false);
		dayText.SetActive (true);
	}

	public void toDay(){
		RenderSettings.ambientLight = Color.white;
		nightText.SetActive (true);
		dayText.SetActive (false);
	}

	void Start () {
		for(int i=0; i < palisades.Length; i++){
			palisades [i].transform.position = targets [Random.Range(0,10)];
		}
	}

	void Update () {
		txBochka.text = numBochka + "";
		if (Input.GetMouseButtonDown (0) && numBochka > 0) {
			Ray r = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit h;
			if (Physics.Raycast (r, out h)) {
				if (h.collider.gameObject.name == "Terrain") {
					GameObject c = Instantiate (bochka);
					c.transform.position = h.point;
					c.name = "bochka";
					numBochka -= 1;
				} else if (h.collider.gameObject.tag == "bochka") {
					Destroy (h.collider.gameObject);
				}
			}
		}

		for (int i = 0; i < zombs.Length; i++) {
			if (sum == -2 && zombs[i].isLife == true) {
				zombs [i].winnerText.SetActive (true);
			}
		}
	}

	public void Play(){
		for (int i = 0; i < zombs.Length; i++) {
			zombs [i].SetState (Zombie.States.Play);
		}
	}
}