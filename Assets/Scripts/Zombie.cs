﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour {

	public enum States{
		Wait, Play
	}
	public States state;

	Animator anim;
	GameManager gm;
	Rigidbody rb;

	public GameObject winnerText;
	public GameObject Ragdoll;

	public Vector3 target;

	public bool isLife = true;

	float timer;

	void Awake () {
		rb = this.GetComponent<Rigidbody> ();
		anim = this.GetComponent<Animator> ();
		gm = GameObject.Find("Main Camera").GetComponent<GameManager> ();
	}

	void Start(){
		target = new Vector3 (4, 6.4f, 1.4f);
	}

	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Ray r = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit h;
			if (Physics.Raycast (r, out h)) {
				if (h.collider.gameObject.name == "Terrain") {
					target = h.point;
				}
			}
		}

		timer += Time.deltaTime;

		if (timer > 5 && isLife == true) {
			RaycastHit h;
			target = new Vector3 (Random.Range (-14, 14), 50, Random.Range (-2.5f, 18));
			if (Physics.Raycast (target, Vector3.down, out h)) {
				target.y = h.point.y;
			}
			timer = 0;
		}

		Vector3 napr = target - this.transform.position;
		this.transform.forward = napr;

		if (napr.magnitude > 0.5f) {
			rb.AddForce (napr.normalized * 4000);	
		}

		if (isLife == false) {
			GameObject c = Instantiate (Ragdoll);
			c.transform.position = this.transform.position;
			Destroy (this.gameObject);
			gm.sum -= 1;
		}
	}
	void OnTriggerEnter (Collider other){
		if (other.tag == "zabor") {
			isLife = false;
		}
		if (other.tag == "Explosion") {
			isLife = false;
		}
	}

	public void SetState(States s){
		state = s;
		if(s == States.Play){
			anim.speed = 1;
		}
		if (s == States.Wait) {
			anim.speed = 0.1f;
		}
	}
}